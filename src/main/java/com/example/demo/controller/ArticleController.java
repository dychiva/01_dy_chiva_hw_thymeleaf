package com.example.demo.controller;


import com.example.demo.custom_annotation.IsAdmin;
import com.example.demo.custom_annotation.IsEditor;
import com.example.demo.custom_annotation.IsReviewer;
import com.example.demo.custom_annotation.IsUser;
import com.example.demo.model.Article;
import com.example.demo.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@IsUser
@RequestMapping("/admin")
@Controller
public class ArticleController {

    private ArticleRepository articleRepository;

    @Autowired
    public ArticleController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }


    @GetMapping("/dashboard")
    @IsAdmin
    public String viewDashboardPage(){
        return "/layouts/dashboard";
    }


    @PostMapping("/article")
    @IsEditor
    public String addArticle(@ModelAttribute Article article, HttpServletRequest request , BindingResult result){


        if (result.hasErrors()) {
            return "/admin/article/article-add";
        }
        article.setUuid(UUID.randomUUID().toString());

        // invoke save article
        articleRepository.save(article);
        String route = "";
        if(request.isUserInRole("ROLE_EDITOR")){
            route = "/admin/article";
        }else if(request.isUserInRole("ROLE_ADMIN")){
            route = "redirect:/admin/review-articles";
        }
        return route;
    }


    @GetMapping("/review-articles")
    @IsReviewer
    public String viewArticleListPage(ModelMap modelMap , @RequestParam(required = false) String lang,
                                      @RequestParam(required = false) String query){
        List<Article> articles = (List<Article>) articleRepository.findAll();
        modelMap.addAttribute("articles", articles);
        return "/admin/article/article-list";
    }


    @GetMapping("/article")
    @IsEditor
    public String viewAddArticlePage(@ModelAttribute Article article, ModelMap modelMap){
        modelMap.addAttribute("articles", article);

        return "/admin/article/article-add";
    }


//    @PostMapping("/admin/articles")
//    @IsEditor
//    public String addArticle(@ModelAttribute @Valid Article article, BindingResult result) {
//
//        if (result.hasErrors()) {
//            return "/admin/article/article-add";
//        }
//        article.setUuid(UUID.randomUUID().toString());
//
//        // invoke save article
//        articleRepository.save(article);
//        return "redirect:/admin/article";
//    }



//    @GetMapping("/admin/review-articles")
//    @IsReviewer
//    public String viewArticlePage(ModelMap modelMap, @RequestParam(required = false) String lang,
//                               @RequestParam(required = false) String query) {
//
//        List<Article> articles = (List<Article>) articleRepository.findAll();
//
//        modelMap.addAttribute("articles", articles);
//        return "/admin/article/article-list";
//    }


//    @GetMapping("/admin/article/add")
//    @IsEditor
//    public String viewAddArticlePage(@ModelAttribute Article article,
//                                     ModelMap modelMap) {
//
//        modelMap.addAttribute("user", article);
//
//        return "/admin/article/article-add";
//    }


//    @GetMapping("/admin/dashboard")
//    @IsAdmin
//    public String viewDashboardPage(){
//        return "/layouts/dashboard";
//    }

}
